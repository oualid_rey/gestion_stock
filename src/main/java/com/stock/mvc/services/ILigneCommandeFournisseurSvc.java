package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entities.*;

public interface ILigneCommandeFournisseurSvc {
	
	public LigneCommandeFournisseur save(LigneCommandeFournisseur entity);
	
	public LigneCommandeFournisseur update(LigneCommandeFournisseur entity);
	
	public List<LigneCommandeFournisseur> selectall();
	
	public List<LigneCommandeFournisseur> selectall(String sortField, String sort);
	
	public LigneCommandeFournisseur getById(long id);
	
	public void remove(long id);
	
	public LigneCommandeFournisseur findOne(String paramName,Object paramValue);
	
	public LigneCommandeFournisseur findOne(String[] paramNames,Object[] paramValues);
	
	public int findCountBy(String paramName,Object paramValue);

}
