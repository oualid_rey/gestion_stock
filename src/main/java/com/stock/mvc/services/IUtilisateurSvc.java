package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entities.Utilisateur;

	public interface IUtilisateurSvc {
	
	public Utilisateur save(Utilisateur entity);
	
	public Utilisateur update(Utilisateur entity);
	
	public List<Utilisateur> selectall();
	
	public List<Utilisateur> selectall(String sortField, String sort);
	
	public Utilisateur getById(long id);
	
	public void remove(long id);
	
	public Utilisateur findOne(String paramName,Object paramValue);
	
	public Utilisateur findOne(String[] paramNames,Object[] paramValues);
	
	public int findCountBy(String paramName,Object paramValue);

}
