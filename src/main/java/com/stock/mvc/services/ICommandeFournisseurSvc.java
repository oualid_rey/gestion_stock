package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entities.*;

public interface ICommandeFournisseurSvc {
	
	public CommandeFournisseur save(CommandeFournisseur entity);
	
	public CommandeFournisseur update(CommandeFournisseur entity);
	
	public List<CommandeFournisseur> selectall();
	
	public List<CommandeFournisseur> selectall(String sortField, String sort);
	
	public CommandeFournisseur getById(long id);
	
	public void remove(long id);
	
	public CommandeFournisseur findOne(String paramName,Object paramValue);
	
	public CommandeFournisseur findOne(String[] paramNames,Object[] paramValues);
	
	public int findCountBy(String paramName,Object paramValue);

}
