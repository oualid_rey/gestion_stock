package com.stock.mvc.services;

import java.util.List;
import com.stock.mvc.entities.Client;

public interface IClientSvc {
	
	public Client save(Client entity);
	
	public Client update(Client entity);
	
	public List<Client> selectall();
	
	public List<Client> selectall(String sortField, String sort);
	
	public Client getById(long id);
	
	public void remove(long id);
	
	public Client findOne(String paramName,Object paramValue);
	
	public Client findOne(String[] paramNames,Object[] paramValues);
	
	public int findCountBy(String paramName,Object paramValue);

}
