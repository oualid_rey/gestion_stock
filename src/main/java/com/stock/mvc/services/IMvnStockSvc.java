package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entities.MvnStock;

public interface IMvnStockSvc {
	
	
	public MvnStock save(MvnStock entity);
	
	public MvnStock update(MvnStock entity);
	
	public List<MvnStock> selectall();
	
	public List<MvnStock> selectall(String sortField, String sort);
	
	public MvnStock getById(long id);
	
	public void remove(long id);
	
	public MvnStock findOne(String paramName,Object paramValue);
	
	public MvnStock findOne(String[] paramNames,Object[] paramValues);
	
	public int findCountBy(String paramName,Object paramValue);

}
