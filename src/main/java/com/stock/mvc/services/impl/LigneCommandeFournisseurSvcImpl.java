package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ILigneCommandeFournisseurDao;
import com.stock.mvc.entities.LigneCommandeClient;
import com.stock.mvc.entities.LigneCommandeFournisseur;
import com.stock.mvc.services.ILigneCommandeClientSvc;
import com.stock.mvc.services.ILigneCommandeFournisseurSvc;

@Transactional
public class LigneCommandeFournisseurSvcImpl implements ILigneCommandeFournisseurSvc{
	
	private ILigneCommandeFournisseurDao dao;
	public void setDao(ILigneCommandeFournisseurDao dao) {
		this.dao = dao;
	}

	@Override
	public LigneCommandeFournisseur save(LigneCommandeFournisseur entity) {
		
		return dao.save(entity);
	}

	@Override
	public LigneCommandeFournisseur update(LigneCommandeFournisseur entity) {

		return dao.update(entity);
	}

	@Override
	public List<LigneCommandeFournisseur> selectall() {
	
		return dao.selectall();
	}

	@Override
	public List<LigneCommandeFournisseur> selectall(String sortField, String sort) {
	
		return dao.selectall(sortField, sort);
	}

	@Override
	public LigneCommandeFournisseur getById(long id) {

		return dao.getById(id);
	}

	@Override
	public void remove(long id) {
		dao.remove(id);
		
	}

	@Override
	public LigneCommandeFournisseur findOne(String paramName, Object paramValue) {

		return dao.findOne(paramName, paramValue);
	}

	@Override
	public LigneCommandeFournisseur findOne(String[] paramNames, Object[] paramValues) {
	
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		
		return dao.findCountBy(paramName, paramValue);
	}

	
	}


	