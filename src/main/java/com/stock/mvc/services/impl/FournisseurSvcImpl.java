package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IFournisseurDao;
import com.stock.mvc.entities.Fournisseur;
import com.stock.mvc.services.IFournisseurSvc;

@Transactional
public class FournisseurSvcImpl implements IFournisseurSvc{
	
	IFournisseurDao dao;
	public void setDao(IFournisseurDao dao) {
		this.dao = dao;
	}

	@Override
	public Fournisseur save(Fournisseur entity) {
		
		return dao.save(entity);
	}

	@Override
	public Fournisseur update(Fournisseur entity) {
		
		return dao.update(entity);
	}

	@Override
	public List<Fournisseur> selectall() {
		
		return dao.selectall();
	}

	@Override
	public List<Fournisseur> selectall(String sortField, String sort) {
		
		return dao.selectall(sortField, sort);
	}

	@Override
	public Fournisseur getById(long id) {
		
		return dao.getById(id);
	}

	@Override
	public void remove(long id) {
		dao.remove(id);
	}

	@Override
	public Fournisseur findOne(String paramName, Object paramValue) {
		
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Fournisseur findOne(String[] paramNames, Object[] paramValues) {
		
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		
		return dao.findCountBy(paramName, paramValue);
	}

}
