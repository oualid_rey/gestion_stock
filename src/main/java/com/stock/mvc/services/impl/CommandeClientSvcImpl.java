package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ICommandeClientDao;
import com.stock.mvc.entities.CommandeClient;
import com.stock.mvc.services.ICommandeClientSvc;

@Transactional
public class CommandeClientSvcImpl implements ICommandeClientSvc{
	
	ICommandeClientDao dao;
	
	public void setDao(ICommandeClientDao dao) {
		this.dao = dao;
	}

	@Override
	public CommandeClient save(CommandeClient entity) {
		
		return dao.save(entity);
	}

	@Override
	public CommandeClient update(CommandeClient entity) {
		
		return dao.update(entity);
	}

	@Override
	public List<CommandeClient> selectall() {
		
		return dao.selectall();
	}

	@Override
	public List<CommandeClient> selectall(String sortField, String sort) {
		
		return dao.selectall(sortField, sort);
	}

	@Override
	public CommandeClient getById(long id) {
		
		return dao.getById(id);
	}

	@Override
	public void remove(long id) {
		dao.remove(id);		
	}

	@Override
	public CommandeClient findOne(String paramName, Object paramValue) {
		
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public CommandeClient findOne(String[] paramNames, Object[] paramValues) {
		
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		
		return dao.findCountBy(paramName, paramValue);
	}

}
