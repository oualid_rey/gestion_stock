package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ILigneVenteDao;
import com.stock.mvc.entities.LigneVente;
import com.stock.mvc.services.ILigneVenteSvc;

@Transactional
public class LigneVenteSvcImpl implements ILigneVenteSvc{
	
	private ILigneVenteDao dao;
	public void setDao(ILigneVenteDao dao) {
		this.dao = dao;
	}

	@Override
	public LigneVente save(LigneVente entity) {
		
		return dao.save(entity);
	}

	@Override
	public LigneVente update(LigneVente entity) {
		
		return dao.update(entity);
	}

	@Override
	public List<LigneVente> selectall() {
		
		return dao.selectall();
	}

	@Override
	public List<LigneVente> selectall(String sortField, String sort) {
		
		return dao.selectall(sortField, sort);
	}

	@Override
	public LigneVente getById(long id) {
		
		return dao.getById(id);
	}

	@Override
	public void remove(long id) {
		dao.remove(id);
	}

	@Override
	public LigneVente findOne(String paramName, Object paramValue) {
		
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public LigneVente findOne(String[] paramNames, Object[] paramValues) {
		
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		
		return dao.findCountBy(paramName, paramValue);
	}

}
