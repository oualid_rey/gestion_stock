package com.stock.mvc.services.impl;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;
import com.stock.mvc.dao.IClientDao;
import com.stock.mvc.entities.Client;
import com.stock.mvc.services.IClientSvc;

@Transactional
public class ClientSvcImpl implements IClientSvc{
	
	private IClientDao dao;
	public void setDao(IClientDao dao) {
		this.dao = dao;
	}

	@Override
	public Client save(Client entity) {
		
		return dao.save(entity);
	}

	@Override
	public Client update(Client entity) {
		
		return dao.update(entity);
	}

	@Override
	public List<Client> selectall() {
		
		return dao.selectall();
	}

	@Override
	public List<Client> selectall(String sortField, String sort) {
		
		return dao.selectall(sortField, sort);
	}

	@Override
	public Client getById(long id) {
		
		return dao.getById(id);
	}

	@Override
	public void remove(long id) {
		dao.remove(id);		
	}

	@Override
	public Client findOne(String paramName, Object paramValue) {
		
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Client findOne(String[] paramNames, Object[] paramValues) {
		
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		// TODO Auto-generated method stub
		return dao.findCountBy(paramName, paramValue);
	}
}
