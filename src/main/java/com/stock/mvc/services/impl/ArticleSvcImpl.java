package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IArticleDao;
import com.stock.mvc.entities.Article;
import com.stock.mvc.services.IArticleSvc;

@Transactional
public class ArticleSvcImpl implements IArticleSvc{
	 
	private IArticleDao dao;
	public void setDao(IArticleDao dao) {
		this.dao = dao;
	}

	@Override
	public Article save(Article entity) {
		
		return dao.save(entity);
	}

	@Override
	public Article update(Article entity) {
		
		return dao.update(entity);
	}

	@Override
	public List<Article> selectall() {
		
		return dao.selectall();
	}

	@Override
	public List<Article> selectall(String sortField, String sort) {
		
		return dao.selectall(sortField, sort);
	}

	@Override
	public Article getById(long id) {
		
		return dao.getById(id);
	}

	@Override
	public void remove(long id) {
		dao.remove(id);	
	}

	@Override
	public Article findOne(String paramName, Object paramValue) {
		
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Article findOne(String[] paramNames, Object[] paramValues) {
		
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		
		return dao.findCountBy(paramName, paramValue);
	}

}
