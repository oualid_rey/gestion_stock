package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IMvnStockDao;
import com.stock.mvc.entities.MvnStock;
import com.stock.mvc.services.IMvnStockSvc;

@Transactional
public class MvnStockSvcImpl implements IMvnStockSvc{
	
	private IMvnStockDao dao;
	public void setDao(IMvnStockDao dao) {
		this.dao = dao;
	}

	@Override
	public MvnStock save(MvnStock entity) {
		
		return dao.save(entity);
	}

	@Override
	public MvnStock update(MvnStock entity) {
		
		return dao.update(entity);
	}

	@Override
	public List<MvnStock> selectall() {
		
		return dao.selectall();
	}

	@Override
	public List<MvnStock> selectall(String sortField, String sort) {
		
		return dao.selectall(sortField, sort);
	}

	@Override
	public MvnStock getById(long id) {
	
		return dao.getById(id);
	}

	@Override
	public void remove(long id) {
		dao.remove(id);
	}

	@Override
	public MvnStock findOne(String paramName, Object paramValue) {
		
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public MvnStock findOne(String[] paramNames, Object[] paramValues) {
		
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		
		return dao.findCountBy(paramName, paramValue);
	}

}
