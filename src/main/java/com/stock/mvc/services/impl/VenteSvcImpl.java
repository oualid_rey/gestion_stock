package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IVenteDao;
import com.stock.mvc.entities.Vente;
import com.stock.mvc.services.IVenteSvc;

@Transactional
public class VenteSvcImpl implements IVenteSvc{
	private IVenteDao dao;
	public void setDao(IVenteDao dao) {
		this.dao = dao;
	}

	@Override
	public Vente save(Vente entity) {
		
		return dao.save(entity);
	}

	@Override
	public Vente update(Vente entity) {
		
		return dao.update(entity);
	}

	@Override
	public List<Vente> selectall() {
		// TODO Auto-generated method stub
		return dao.selectall();
	}

	@Override
	public List<Vente> selectall(String sortField, String sort) {
		
		return dao.selectall(sortField, sort);
	}

	@Override
	public Vente getById(long id) {
		
		return dao.getById(id);
	}

	@Override
	public void remove(long id) {
		dao.remove(id);
	}

	@Override
	public Vente findOne(String paramName, Object paramValue) {
		
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Vente findOne(String[] paramNames, Object[] paramValues) {
		
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		
		return dao.findCountBy(paramName, paramValue);
	}

}
