package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.ILigneCommandeClientDao;
import com.stock.mvc.entities.LigneCommandeClient;
import com.stock.mvc.services.ILigneCommandeClientSvc;

@Transactional
public class LigneCommandeClientSvcImpl implements ILigneCommandeClientSvc{
	ILigneCommandeClientDao dao;
	public void setDao(ILigneCommandeClientDao dao) {
		this.dao = dao;
	}

	@Override
	public LigneCommandeClient save(LigneCommandeClient entity) {
		
		return dao.save(entity);
	}

	@Override
	public LigneCommandeClient update(LigneCommandeClient entity) {
		
		return dao.update(entity);
	}

	@Override
	public List<LigneCommandeClient> selectall() {
		
		return dao.selectall();
	}

	@Override
	public List<LigneCommandeClient> selectall(String sortField, String sort) {
		
		return dao.selectall(sortField, sort);
	}

	@Override
	public LigneCommandeClient getById(long id) {
		
		return dao.getById(id);
	}

	@Override
	public void remove(long id) {
		dao.remove(id);	
	}

	@Override
	public LigneCommandeClient findOne(String paramName, Object paramValue) {
		
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public LigneCommandeClient findOne(String[] paramNames, Object[] paramValues) {
		
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		
		return dao.findCountBy(paramName, paramValue);
	}

}
