package com.stock.mvc.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.stock.mvc.dao.IUtilisateurDao;
import com.stock.mvc.entities.Utilisateur;
import com.stock.mvc.services.IUtilisateurSvc;

@Transactional
public class UtilisateurSvcImpl implements IUtilisateurSvc{
	private IUtilisateurDao dao;
	public void setDao(IUtilisateurDao dao) {
		this.dao = dao;
	}

	@Override
	public Utilisateur save(Utilisateur entity) {
		
		return dao.save(entity);
	}

	@Override
	public Utilisateur update(Utilisateur entity) {
		
		return dao.update(entity);
	}

	@Override
	public List<Utilisateur> selectall() {
		
		return dao.selectall();
	}

	@Override
	public List<Utilisateur> selectall(String sortField, String sort) {
		
		return dao.selectall(sortField, sort);
	}

	@Override
	public Utilisateur getById(long id) {
		
		return dao.getById(id);
	}

	@Override
	public void remove(long id) {
		dao.remove(id);
	}

	@Override
	public Utilisateur findOne(String paramName, Object paramValue) {
		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Utilisateur findOne(String[] paramNames, Object[] paramValues) {
		return dao.findOne(paramNames, paramValues);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		
		return dao.findCountBy(paramName, paramValue);
	}

}
