package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entities.Article;

public interface IArticleSvc {
	
	public Article save(Article entity);
	
	public Article update(Article entity);
	
	public List<Article> selectall();
	
	public List<Article> selectall(String sortField, String sort);
	
	public Article getById(long id);
	
	public void remove(long id);
	
	public Article findOne(String paramName,Object paramValue);
	
	public Article findOne(String[] paramNames,Object[] paramValues);
	
	public int findCountBy(String paramName,Object paramValue);

}
