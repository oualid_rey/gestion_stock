package com.stock.mvc.services;

import java.util.List;

import com.stock.mvc.entities.*;

public interface IFournisseurSvc {
	
	public Fournisseur save(Fournisseur entity);
	
	public Fournisseur update(Fournisseur entity);
	
	public List<Fournisseur> selectall();
	
	public List<Fournisseur> selectall(String sortField, String sort);
	
	public Fournisseur getById(long id);
	
	public void remove(long id);
	
	public Fournisseur findOne(String paramName,Object paramValue);
	
	public Fournisseur findOne(String[] paramNames,Object[] paramValues);
	
	public int findCountBy(String paramName,Object paramValue);

}
