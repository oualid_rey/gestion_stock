package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="utilisateur")
public class Utilisateur implements Serializable{
	
	@Id
	@Column(name="idutilisateur")
	@GeneratedValue
	private long idUtilisateur;
	private String nom;
	private String prenom;
	private String motDepasse;
	private String mail;
	private String photo;

	public long getIdUtilisateur() {
		return idUtilisateur;
	}

	public void setIdUtilisateur(long idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMotDepasse() {
		return motDepasse;
	}

	public void setMotDepasse(String motDepasse) {
		this.motDepasse = motDepasse;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	
}
