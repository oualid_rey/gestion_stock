package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Lignecommandefournisseur")
public class LigneCommandeFournisseur  implements Serializable{
	
	@Id
	@Column(name="idlignecommandefournnisseur")
	@GeneratedValue	
	private long idLigneCommandeFournnisseur;
	//----------------------------------------------------------
		@ManyToOne
		@JoinColumn(name="idArticle")
		private Article article;
		//---------------------------------------------------------
		@ManyToOne
		@JoinColumn(name="idCommandeFournisseur")
		private CommandeFournisseur commandefournisseur;
		//---------------------------------------------------------

	public long getIdLigneCommandeFournnisseur() {
		return idLigneCommandeFournnisseur;
	}

	public void setIdLigneCommandeFournnisseur(long idLigneCommandeFournnisseur) {
		this.idLigneCommandeFournnisseur = idLigneCommandeFournnisseur;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeFournisseur getCommandefournisseur() {
		return commandefournisseur;
	}

	public void setCommandefournisseur(CommandeFournisseur commandefournisseur) {
		this.commandefournisseur = commandefournisseur;
	}
	
}
