package com.stock.mvc.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;


@Entity
@Table(name="commandeclient")
public class CommandeClient implements Serializable{
	
	@Id
	@GeneratedValue
	@Column(name="idcommandeclient")
	private long idCommandeClient;
	private String codeCommandeClient;	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCommandeClient;
	//-------------------------------------------------------------------
	@ManyToOne
	@JoinColumn(name="idClient")
	private Client client;
	//-----------------------------------------------------------------------------------
	@OneToMany(mappedBy="commandeclient")
	private List<LigneCommandeClient> lignecommandeclient;
	//---------------------------------------------------------------------
	
	
	public long getIdCommandeClient() {
		return idCommandeClient;
	}

	public void setIdCommandeClient(long idCommandeClient) {
		this.idCommandeClient = idCommandeClient;
	}

	public String getCodeCommandeClient() {
		return codeCommandeClient;
	}

	public void setCodeCommandeClient(String codeCommandeClient) {
		this.codeCommandeClient = codeCommandeClient;
	}

	public Date getDateCommandeClient() {
		return dateCommandeClient;
	}

	public void setDateCommandeClient(Date dateCommandeClient) {
		this.dateCommandeClient = dateCommandeClient;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<LigneCommandeClient> getLignecommandeclient() {
		return lignecommandeclient;
	}

	public void setLignecommandeclient(List<LigneCommandeClient> lignecommandeclient) {
		this.lignecommandeclient = lignecommandeclient;
	}
	
}
