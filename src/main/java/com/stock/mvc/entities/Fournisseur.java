package com.stock.mvc.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="fournisseur")
public class Fournisseur implements Serializable{
	
	@Id
	@Column(name="idfournisseur")
	@GeneratedValue
	private long idFournisseur;
	private String nom;
	private String prenom;
	private String mail;
	private String telephone;
	private String adresse;
	private String photo;
	//----------------------------------------------------------
	@OneToMany(mappedBy="fournisseur")
	private List<CommandeFournisseur> commandefournisseur;
	//------------------------------------------------------------
	public Fournisseur() {
		super();
		
	}

	public long getIdFournisseur() {
		return idFournisseur;
	}

	public void setIdFournisseur(long idFournisseur) {
		this.idFournisseur = idFournisseur;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public List<CommandeFournisseur> getCommandefournisseur() {
		return commandefournisseur;
	}

	public void setCommandefournisseur(List<CommandeFournisseur> commandefournisseur) {
		this.commandefournisseur = commandefournisseur;
	}
	
}
