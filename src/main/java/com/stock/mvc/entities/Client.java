package com.stock.mvc.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="client")
public class Client implements Serializable{
	
	
	@Id
	@Column(name="idclient")
	@GeneratedValue
	private long idClient;
	private String nom;
	private String prenom;
	private String mail;
	private String telephone;
	private String adresse;
	private String photo;
	//------------------------------------------------------------------
	@OneToMany(mappedBy="client")
	private List<CommandeClient> commandeclient;
	//------------------------------------------------------- 	
	public Client() {	
	}

	public long getIdClient() {
		return idClient;
	}

	public void setIdClient(long idClient) {
		this.idClient = idClient;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public List<CommandeClient> getCommandeclient() {
		return commandeclient;
	}

	public void setCommandeclient(List<CommandeClient> commandeclient) {
		this.commandeclient = commandeclient;
	}
	
}
