package com.stock.mvc.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="vente")
public class Vente implements Serializable{
	
	@Id
	@Column(name="idvente")
	@GeneratedValue
	private long idVente;
	private String codeVente;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateVente;
	//-----------------------------------
	@OneToMany(mappedBy="vente")
	private List <LigneVente> lignevente;
	//-----------------------------------------
	public long getIdVente() {
		return idVente;
	}

	public void setIdVente(long idVente) {
		this.idVente = idVente;
	}

	public String getCodeVente() {
		return codeVente;
	}

	public void setCodeVente(String codeVente) {
		this.codeVente = codeVente;
	}

	public Date getDateVente() {
		return dateVente;
	}

	public void setDateVente(Date dateVente) {
		this.dateVente = dateVente;
	}

	public List<LigneVente> getLignevente() {
		return lignevente;
	}

	public void setLignevente(List<LigneVente> lignevente) {
		this.lignevente = lignevente;
	}
}
