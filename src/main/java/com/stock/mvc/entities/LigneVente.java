package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="lignevente")
public class LigneVente implements Serializable{
	@Id
	@GeneratedValue
	@Column(name="idlignevente")
	private long idLigneVente;
	//---------------------------------------
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	//--------------------------------------
	@ManyToOne
	@JoinColumn(name="idvente")
	private Vente vente;
	//---------------------------------------
	public long getIdLigneVente() {
		return idLigneVente;
	}

	public void setIdLigneVente(long idLigneVente) {
		this.idLigneVente = idLigneVente;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
}
