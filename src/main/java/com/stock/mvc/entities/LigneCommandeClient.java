package com.stock.mvc.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="lignecommandeclient")
public class LigneCommandeClient implements Serializable{
	
	@Id
	@Column(name="idlignecommandeclient")
	@GeneratedValue	
	private long idLigneCommandeClient;
	//----------------------------------------------------------
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	//---------------------------------------------------------
	@ManyToOne
	@JoinColumn(name="idCommandeClient")
	private CommandeClient commandeclient;
	//---------------------------------------------------------
	public long getIdLigneCommandeClient() {
		return idLigneCommandeClient;
	}

	public void setIdLigneCommandeClient(long idLigneCommandeClient) {
		this.idLigneCommandeClient = idLigneCommandeClient;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeClient getCommandeclient() {
		return commandeclient;
	}

	public void setCommandeclient(CommandeClient commandeclient) {
		this.commandeclient = commandeclient;
	}

}
