package com.stock.mvc.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name="commandefournisseur")
public class CommandeFournisseur implements Serializable{
	
	@Id
	@Column(name="idcommandefournisseur")
	@GeneratedValue
	private long idCommandeFournisseur;
	private String codeCommandeFourniseur;	
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCommandeFournisseur;
	//-------------------------------------------------------------------
	@ManyToOne
	@JoinColumn(name="idFournisseur")
	private Fournisseur fournisseur;
	//-----------------------------------------------------------------------------------
	@OneToMany(mappedBy="commandefournisseur")
	private List<LigneCommandeFournisseur> lignecommandefournisseur;
	//---------------------------------------------------------------------
	public long getIdCommandeFournisseur() {
		return idCommandeFournisseur;
	}

	public void setIdCommandeFournisseur(long idCommandeFournisseur) {
		this.idCommandeFournisseur = idCommandeFournisseur;
	}

	public String getCodeCommandeFourniseur() {
		return codeCommandeFourniseur;
	}

	public void setCodeCommandeFourniseur(String codeCommandeFourniseur) {
		this.codeCommandeFourniseur = codeCommandeFourniseur;
	}

	public Date getDateCommandeFournisseur() {
		return dateCommandeFournisseur;
	}

	public void setDateCommandeFournisseur(Date dateCommandeFournisseur) {
		this.dateCommandeFournisseur = dateCommandeFournisseur;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public List<LigneCommandeFournisseur> getLignecommandefournisseur() {
		return lignecommandefournisseur;
	}

	public void setLignecommandefournisseur(List<LigneCommandeFournisseur> lignecommandefournisseur) {
		this.lignecommandefournisseur = lignecommandefournisseur;
	}

}
