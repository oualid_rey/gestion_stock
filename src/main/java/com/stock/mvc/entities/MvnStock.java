package com.stock.mvc.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;


@Entity
@Table(name="mvnstock")
public class MvnStock implements Serializable{
	
	public static final int ENTREE=1;
	public static final int SORTEE=2;	
	@Id
	@GeneratedValue
	@Column(name="idmvnstock")
	private long idMvnStock;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateMvnStock;
	private BigDecimal quantite;	
	private int typeMvnStock;
	
	@ManyToOne
	@JoinColumn(name="idArticle")
	private Article article;
	
	public long getIdMvnStock() {
		return idMvnStock;
	}

	public void setIdMvnStock(long idMvnStock) {
		this.idMvnStock = idMvnStock;
	}

	public Date getDateMvnStock() {
		return dateMvnStock;
	}

	public void setDateMvnStock(Date dateMvnStock) {
		this.dateMvnStock = dateMvnStock;
	}

	public BigDecimal getQuantite() {
		return quantite;
	}

	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}

	public int getTypeMvnStock() {
		return typeMvnStock;
	}

	public void setTypeMvnStock(int typeMvnStock) {
		this.typeMvnStock = typeMvnStock;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}
	
	
}
