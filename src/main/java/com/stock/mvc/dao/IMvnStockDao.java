package com.stock.mvc.dao;

import com.stock.mvc.entities.MvnStock;

public interface IMvnStockDao extends IGenericDao<MvnStock> {

}
