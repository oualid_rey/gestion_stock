package com.stock.mvc.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.stock.mvc.dao.IGenericDao;

@SuppressWarnings("unchecked")
public class GenericDaoImpl<E> implements IGenericDao<E>{
	@PersistenceContext
	EntityManager EM;
	
	private Class<E> type;
		
	public Class<E> getType() {
		return type;
	}

	public GenericDaoImpl() {
		Type t=getClass().getGenericSuperclass();
		ParameterizedType pt=(ParameterizedType)t;
		type=(Class<E>) pt.getActualTypeArguments()[0];
	}
	
	@Override
	public E save(E entity) {
		EM.persist(entity);
		return entity;
	}

	@Override
	public E update(E entity) {
		EM.merge(entity);
		return entity;
	}

	@Override
	public List<E> selectall() {
		Query query=EM.createQuery("select t from "+type.getSimpleName()+"t");
		return query.getResultList();
	}

	@Override
	public List<E> selectall(String sortField, String sort) {
		Query query=EM.createQuery("select t from "+type.getSimpleName()+"t order by "+ sortField + " " + sort);
		return query.getResultList();
	}

	@Override
	public E getById(long id) {
		return EM.find(type, id);
		 
	}

	@Override
	public void remove(long id) {
		E tab=EM.getReference(type, id);
		EM.remove(tab);		
	}

	@Override
	public E findOne(String paramName, Object paramValue) {
		Query query=EM.createQuery("select t from "+ type.getSimpleName() + "t  where " + paramName + "= :x");
		query.setParameter(paramName, paramValue);
		return query.getResultList().size() > 0 ?  (E) query.getResultList().get(0) : null;
	}

	@Override
	public E findOne(String[] paramNames, Object[] paramValues) {
		if (paramNames.length != paramValues.length) {
			return null;
		}
		String queryString="select e from "+type.getSimpleName()+" e where ";
		int len=paramNames.length;
		for (int i = 0; i < len; i++) {
			queryString +=" e."+paramNames[i]+"= :x"+i;
			if ((i+1)<len) {
				queryString +=" and";
			}
		}
		Query query=EM.createQuery(queryString);
		for (int i = 0; i < paramValues.length; i++) {
			query.setParameter("x"+i, paramValues[i]);
		}
		return query.getResultList().size() > 0 ?  (E) query.getResultList().get(0) : null;
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {
		Query query=EM.createQuery("select t from "+ type.getSimpleName() + "t  where " + paramName + "= :x");
		query.setParameter(paramName, paramValue);
		return query.getResultList().size() > 0 ? ((Long) query.getSingleResult()).intValue() : 0;
	}

}
